﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;

        private readonly IDistributedCache _distributedCache;

        public PreferencesController(IRepository<Preference> preferencesRepository, IDistributedCache distributedCache)
        {
            _preferencesRepository = preferencesRepository;
            _distributedCache = distributedCache;
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            IEnumerable<Preference> preferences;

            var cachedPreferences = await _distributedCache.GetStringAsync("Preferences");

            if (cachedPreferences != null)
            {
                preferences = JsonConvert.DeserializeObject<List<Preference>>(cachedPreferences);
            }
            else
            {
                preferences = await _preferencesRepository.GetAllAsync();

                var options = new DistributedCacheEntryOptions()
                                    .SetSlidingExpiration(TimeSpan.FromMinutes(5))
                                    .SetAbsoluteExpiration(TimeSpan.FromHours(5));

                await _distributedCache.SetStringAsync("Preferences", JsonConvert.SerializeObject(preferences), options);
            }

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);
        }
    }
}